package me.yanglw.java.pipeline;

/**
 * Created by yanglw on 2018-10-25.
 */
public interface OutboundHandler extends Handler {

    void bind(HandlerContext ctx) throws Exception;

    void close(HandlerContext ctx) throws Exception;

    void connect(HandlerContext ctx) throws Exception;

    void disconnect(HandlerContext ctx) throws Exception;

    void write(HandlerContext ctx, Object msg) throws Exception;

    void flush(HandlerContext ctx) throws Exception;
}

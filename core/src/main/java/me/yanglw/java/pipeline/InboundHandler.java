package me.yanglw.java.pipeline;

/**
 * Created by yanglw on 2018-10-25.
 */
public interface InboundHandler extends Handler {
    void active(HandlerContext ctx) throws Exception;

    void inactive(HandlerContext ctx) throws Exception;

    void read(HandlerContext ctx, Object msg) throws Exception;
}

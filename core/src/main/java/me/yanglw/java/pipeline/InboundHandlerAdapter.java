package me.yanglw.java.pipeline;

/**
 * Created by yanglw on 2018-10-25.
 */
public class InboundHandlerAdapter extends HandlerAdapter implements InboundHandler {
    @Override
    public void handlerAdded(HandlerContext ctx) throws Exception {
    }

    @Override
    public void handlerRemoved(HandlerContext ctx) throws Exception {
    }

    @Override
    public void exceptionCaught(HandlerContext ctx, Throwable cause) throws Exception {
        ctx.fireExceptionCaught(cause);
    }

    @Override
    public void active(HandlerContext ctx) throws Exception {
        ctx.fireActive();
    }

    @Override
    public void inactive(HandlerContext ctx) throws Exception {
        ctx.fireInactive();
    }

    @Override
    public void read(HandlerContext ctx, Object msg) throws Exception {
        ctx.fireRead(msg);
    }
}

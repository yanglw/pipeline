package me.yanglw.java.pipeline;

/**
 * Created by yanglw on 2018-10-25.
 */
public interface Handler {
    void handlerAdded(HandlerContext ctx) throws Exception;

    void handlerRemoved(HandlerContext ctx) throws Exception;

    void exceptionCaught(HandlerContext ctx, Throwable cause) throws Exception;
}

package me.yanglw.java.pipeline;

/**
 * Created by yanglw on 2018-10-25.
 */
public interface Pipeline extends OutboundInvoker, InboundInvoker {
    void addLast(Handler handler);

    void addLast(String name, Handler handler);

    void addFirst(Handler handler);

    void addFirst(String name, Handler handler);

    void remove(Handler handler);

    void remove(String name);
}

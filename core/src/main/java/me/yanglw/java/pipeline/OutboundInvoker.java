package me.yanglw.java.pipeline;

/**
 * Created by yanglw on 2018-10-25.
 */
interface OutboundInvoker {
    void bind();

    void close();

    void connect();

    void disconnect();

    void write(Object msg);

    void flush();

    void writeAndFlush(Object msg);
}

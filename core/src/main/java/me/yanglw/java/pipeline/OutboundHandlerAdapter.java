package me.yanglw.java.pipeline;

/**
 * Created by yanglw on 2018-10-25.
 */
public class OutboundHandlerAdapter extends HandlerAdapter implements OutboundHandler {

    @Override
    public void bind(HandlerContext ctx) throws Exception {
        ctx.bind();
    }

    @Override
    public void connect(HandlerContext ctx) throws Exception {
        ctx.connect();
    }

    @Override
    public void disconnect(HandlerContext ctx) throws Exception {
        ctx.disconnect();
    }

    @Override
    public void close(HandlerContext ctx) throws Exception {
        ctx.close();
    }

    @Override
    public void write(HandlerContext ctx, Object msg) throws Exception {
        ctx.write(msg);
    }

    @Override
    public void flush(HandlerContext ctx) throws Exception {
        ctx.flush();
    }
}

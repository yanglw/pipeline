package me.yanglw.java.pipeline;

/**
 * Created by yanglw on 2018-10-25.
 */
public class HandlerAdapter implements Handler {
    @Override
    public void handlerAdded(HandlerContext ctx) throws Exception {
    }

    @Override
    public void handlerRemoved(HandlerContext ctx) throws Exception {
    }

    @Override
    public void exceptionCaught(HandlerContext ctx, Throwable cause) throws Exception {
        ctx.fireExceptionCaught(cause);
    }
}

package me.yanglw.java.pipeline;

/**
 * Created by yanglw on 2018-10-25.
 */
class LinkedHandlerContext implements HandlerContext {
    private Pipeline mPipeline;
    private String mName;
    private Handler mHandler;

    private boolean mRemove;

    LinkedHandlerContext prev;
    LinkedHandlerContext next;

    LinkedHandlerContext(Pipeline pipeline, String name, Handler handler) {
        mPipeline = pipeline;
        mName = name;
        mHandler = handler;
    }

    @Override
    public Pipeline pipeline() {
        return mPipeline;
    }

    @Override
    public String name() {
        return mName;
    }

    @Override
    public Handler handler() {
        return mHandler;
    }

    @Override
    public boolean isRemoved() {
        return mRemove;
    }

    void setAdded() {
        mRemove = false;
    }

    void setRemoved() {
        mRemove = true;
    }

    @Override
    public HandlerContext fireActive() {
        invokeActive(next);
        return this;
    }

    static void invokeActive(HandlerContext context) {
        Handler handler = context.handler();
        if (handler instanceof InboundHandler) {
            try {
                ((InboundHandler) handler).active(context);
            } catch (Exception e) {
                invokeExceptionCaught(context, e);
            }
        } else {
            context.fireActive();
        }
    }

    @Override
    public HandlerContext fireInactive() {
        invokeInactive(next);
        return this;
    }

    static void invokeInactive(HandlerContext context) {
        Handler handler = context.handler();
        if (handler instanceof InboundHandler) {
            try {
                ((InboundHandler) handler).inactive(context);
            } catch (Exception e) {
                invokeExceptionCaught(context, e);
            }
        } else {
            context.fireInactive();
        }
    }

    @Override
    public HandlerContext fireRead(Object msg) {
        invokeRead(next, msg);
        return this;
    }

    static void invokeRead(HandlerContext context, Object msg) {
        Handler handler = context.handler();
        if (handler instanceof InboundHandler) {
            try {
                ((InboundHandler) handler).read(context, msg);
            } catch (Exception e) {
                invokeExceptionCaught(context, e);
            }
        } else {
            context.fireRead(msg);
        }
    }

    @Override
    public HandlerContext fireExceptionCaught(Throwable cause) {
        invokeExceptionCaught(next, cause);
        return this;
    }

    static void invokeExceptionCaught(HandlerContext context, Throwable cause) {
        try {
            context.handler().exceptionCaught(context, cause);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void bind() {
        invokeBind(prev);
    }

    static void invokeBind(HandlerContext context) {
        Handler handler = context.handler();
        if (handler instanceof OutboundHandler) {
            try {
                ((OutboundHandler) handler).bind(context);
            } catch (Exception e) {
                invokeExceptionCaught(context, e);
            }
        } else {
            context.bind();
        }
    }

    @Override
    public void close() {
        invokeClose(prev);
    }

    static void invokeClose(HandlerContext context) {
        Handler handler = context.handler();
        if (handler instanceof OutboundHandler) {
            try {
                ((OutboundHandler) handler).close(context);
            } catch (Exception e) {
                invokeExceptionCaught(context, e);
            }
        } else {
            context.close();
        }
    }

    @Override
    public void connect() {
        invokeConnect(prev);
    }

    static void invokeConnect(HandlerContext context) {
        Handler handler = context.handler();
        if (handler instanceof OutboundHandler) {
            try {
                ((OutboundHandler) handler).connect(context);
            } catch (Exception e) {
                invokeExceptionCaught(context, e);
            }
        } else {
            context.connect();
        }
    }

    @Override
    public void disconnect() {
        invokeDisconnect(prev);
    }

    static void invokeDisconnect(HandlerContext context) {
        Handler handler = context.handler();
        if (handler instanceof OutboundHandler) {
            try {
                ((OutboundHandler) handler).disconnect(context);
            } catch (Exception e) {
                invokeExceptionCaught(context, e);
            }
        } else {
            context.disconnect();
        }
    }

    @Override
    public void write(Object msg) {
        invokeWrite(prev, msg);
    }

    static void invokeWrite(HandlerContext context, Object msg) {
        Handler handler = context.handler();
        if (handler instanceof OutboundHandler) {
            try {
                ((OutboundHandler) handler).write(context, msg);
            } catch (Exception e) {
                invokeExceptionCaught(context, e);
            }
        } else {
            context.write(msg);
        }
    }

    @Override
    public void flush() {
        invokeFlush(prev);
    }

    static void invokeFlush(HandlerContext context) {
        Handler handler = context.handler();
        if (handler instanceof OutboundHandler) {
            try {
                ((OutboundHandler) handler).flush(context);
            } catch (Exception e) {
                invokeExceptionCaught(context, e);
            }
        } else {
            context.flush();
        }
    }

    @Override
    public void writeAndFlush(Object msg) {
        write(msg);
        flush();
    }
}

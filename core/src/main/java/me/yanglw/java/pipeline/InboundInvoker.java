package me.yanglw.java.pipeline;

/**
 * Created by yanglw on 2018-10-25.
 */
interface InboundInvoker {
    InboundInvoker fireActive();

    InboundInvoker fireInactive();

    InboundInvoker fireRead(Object msg);

    InboundInvoker fireExceptionCaught(Throwable cause);
}

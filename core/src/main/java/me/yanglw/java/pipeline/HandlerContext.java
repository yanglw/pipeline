package me.yanglw.java.pipeline;

/**
 * Created by yanglw on 2018-10-25.
 */
public interface HandlerContext extends InboundInvoker, OutboundInvoker {
    String name();

    Handler handler();

    boolean isRemoved();

    Pipeline pipeline();
}

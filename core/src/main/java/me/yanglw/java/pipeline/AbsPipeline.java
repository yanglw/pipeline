package me.yanglw.java.pipeline;

/**
 * Created by yanglw on 2018-10-25.
 */
public abstract class AbsPipeline implements Pipeline {
    private LinkedHandlerContext head;
    private LinkedHandlerContext foot;

    public AbsPipeline() {
        head = new LinkedHandlerContext(this, "HeadHandler", getHeader());
        foot = new LinkedHandlerContext(this, "FootHandler", getFooter());
        head.next = foot;
        foot.prev = head;
    }

    protected abstract OutboundHandler getHeader();

    protected abstract InboundHandler getFooter();

    @Override
    public Pipeline fireActive() {
        LinkedHandlerContext.invokeActive(head);
        return this;
    }

    @Override
    public Pipeline fireInactive() {
        LinkedHandlerContext.invokeInactive(head);
        return this;
    }

    @Override
    public Pipeline fireRead(Object msg) {
        LinkedHandlerContext.invokeRead(head, msg);
        return this;
    }

    @Override
    public Pipeline fireExceptionCaught(Throwable cause) {
        LinkedHandlerContext.invokeExceptionCaught(head, cause);
        return this;
    }

    @Override
    public void bind() {
        LinkedHandlerContext.invokeBind(foot);
    }

    @Override
    public void close() {
        LinkedHandlerContext.invokeClose(foot);
    }

    @Override
    public void connect() {
        LinkedHandlerContext.invokeConnect(foot);
    }

    @Override
    public void disconnect() {
        LinkedHandlerContext.invokeDisconnect(foot);
    }

    @Override
    public void write(Object msg) {
        LinkedHandlerContext.invokeWrite(foot, msg);
    }

    @Override
    public void flush() {
        LinkedHandlerContext.invokeFlush(foot);
    }

    @Override
    public void writeAndFlush(Object msg) {
        write(msg);
        flush();
    }

    protected LinkedHandlerContext createContext(String name, Handler handler) {
        return new LinkedHandlerContext(this, name, handler);
    }

    @Override
    public void addLast(Handler handler) {
        addLast(null, handler);
    }

    @Override
    public void addLast(String name, Handler handler) {
        if (handler == null) {
            throw new RuntimeException("handler can not be null.");
        }
        if (isAdded(handler)) {
            return;
        }

        LinkedHandlerContext context = createContext(name, handler);
        insertHandler(foot.prev, context, foot);
    }

    @Override
    public void addFirst(Handler handler) {
        addFirst(null, handler);
    }

    @Override
    public void addFirst(String name, Handler handler) {
        if (handler == null) {
            throw new RuntimeException("handler can not be null.");
        }
        if (isAdded(handler)) {
            return;
        }

        LinkedHandlerContext context = createContext(name, handler);
        insertHandler(head, context, head.next);
    }

    private boolean isAdded(Handler handler) {
        LinkedHandlerContext next = head.next;
        while (next != foot) {
            if (next.handler() == handler) {
                return true;
            }
            next = next.next;
        }
        return false;
    }

    private void insertHandler(LinkedHandlerContext first, LinkedHandlerContext center,
                               LinkedHandlerContext last) {
        first.next = center;
        center.prev = first;
        center.next = last;
        last.prev = center;

        try {
            center.handler().handlerAdded(center);
            center.setAdded();
        } catch (Exception e) {
            removeHandler(center);
            try {
                center.handler().handlerRemoved(center);
            } catch (Exception e1) {
                e1.printStackTrace();
            } finally {
                center.setRemoved();
            }
            fireExceptionCaught(e);
        }
    }

    @Override
    public void remove(Handler handler) {
        LinkedHandlerContext next = head.next;
        while (next != foot) {
            if (next == handler) {
                removeHandler(next);
                callHandlerRemoved(next);
            }
        }
    }

    @Override
    public void remove(String name) {
        LinkedHandlerContext next = head.next;
        while (next != foot) {
            String name1 = next.name();
            if ((name == null && name1 == null)
                || (name != null && name.equals(name1))) {
                removeHandler(next);
                callHandlerRemoved(next);
            }
        }
    }

    private void removeHandler(LinkedHandlerContext context) {
        LinkedHandlerContext prev = context.prev;
        context.prev = context.next;
        context.next = prev;
    }

    private void callHandlerRemoved(LinkedHandlerContext context) {
        try {
            context.handler().handlerRemoved(context);
        } catch (Exception e) {
            fireExceptionCaught(e);
        } finally {
            context.setRemoved();
        }
    }
}

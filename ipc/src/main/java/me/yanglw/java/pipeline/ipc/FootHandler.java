package me.yanglw.java.pipeline.ipc;

import me.yanglw.java.pipeline.HandlerContext;
import me.yanglw.java.pipeline.InboundHandler;

/**
 * Created by yanglw on 2018-10-25.
 */
class FootHandler implements InboundHandler {
    private IpcPigeon mPigeon;

    public FootHandler(IpcPigeon pigeon) {
        mPigeon = pigeon;
    }

    @Override
    public void handlerAdded(HandlerContext ctx) throws Exception {}

    @Override
    public void handlerRemoved(HandlerContext ctx) throws Exception {}

    @Override
    public void exceptionCaught(HandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
    }

    @Override
    public void active(HandlerContext ctx) throws Exception {}

    @Override
    public void inactive(HandlerContext ctx) throws Exception {}

    @Override
    public void read(HandlerContext ctx, Object msg) throws Exception {}
}

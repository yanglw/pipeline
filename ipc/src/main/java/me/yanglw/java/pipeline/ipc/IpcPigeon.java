package me.yanglw.java.pipeline.ipc;

public interface IpcPigeon {
    void bind();

    void close();

    void connect();

    void disconnect();

    void write(Object msg);

    void flush();
}
package me.yanglw.java.pipeline.ipc;

import me.yanglw.java.pipeline.HandlerContext;
import me.yanglw.java.pipeline.OutboundHandler;
import me.yanglw.java.pipeline.OutboundHandlerAdapter;

/**
 * Created by yanglw on 2018-10-25.
 */
class HeadHandler extends OutboundHandlerAdapter implements OutboundHandler {
    private IpcPigeon mPigeon;
    public HeadHandler(IpcPigeon pigeon) {
        mPigeon = pigeon;
    }

    @Override
    public void bind(HandlerContext ctx) throws Exception {
        mPigeon.bind();
    }

    @Override
    public void close(HandlerContext ctx) throws Exception {
        mPigeon.close();
    }

    @Override
    public void connect(HandlerContext ctx) throws Exception {
        mPigeon.connect();
    }

    @Override
    public void disconnect(HandlerContext ctx) throws Exception {
        mPigeon.disconnect();
    }

    @Override
    public void write(HandlerContext ctx, Object msg) throws Exception {
        mPigeon.write(msg);
    }

    @Override
    public void flush(HandlerContext ctx) throws Exception {
        mPigeon.flush();
    }
}

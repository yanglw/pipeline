package me.yanglw.java.pipeline.ipc;

import me.yanglw.java.pipeline.AbsPipeline;
import me.yanglw.java.pipeline.InboundHandler;
import me.yanglw.java.pipeline.OutboundHandler;

/**
 * Created by yanglw on 2018-10-25.
 */
public class IpcPipeline extends AbsPipeline {
    private IpcPigeon mPigeon;

    public IpcPipeline(PigeonFactory pigeonFactory) {
        mPigeon = pigeonFactory.create(this);
    }

    public IpcPigeon pigeon() {
        return mPigeon;
    }

    @Override
    protected OutboundHandler getHeader() {
        return new HeadHandler(mPigeon);
    }

    @Override
    protected InboundHandler getFooter() {
        return new FootHandler(mPigeon);
    }

    public interface PigeonFactory {
        IpcPigeon create(IpcPipeline ipc);
    }
}

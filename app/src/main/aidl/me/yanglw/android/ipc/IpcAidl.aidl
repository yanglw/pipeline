// IpcAidlInterface.aidl
package me.yanglw.android.ipc;

import me.yanglw.android.ipc.CallbackAidl;
import me.yanglw.android.ipc.IpcEvent;

// Declare any non-default types here with import statements
interface IpcAidl {
    void send(in IpcEvent event);

    void register(CallbackAidl callback);

    void unregister(CallbackAidl callback);
}

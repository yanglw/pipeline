// CallbackAidlInterface.aidl
package me.yanglw.android.ipc;

import me.yanglw.android.ipc.IpcEvent;
// Declare any non-default types here with import statements

interface CallbackAidl {
    void send(in IpcEvent event);
}

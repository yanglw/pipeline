package me.yanglw.android.ipc;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import me.yanglw.android.ipc.client.AidlClientIpcPigeon;
import me.yanglw.java.pipeline.HandlerContext;
import me.yanglw.java.pipeline.InboundHandlerAdapter;
import me.yanglw.java.pipeline.ipc.IpcPipeline;

public class MainActivity extends Activity {
    private EditText mWhat;
    private EditText mText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final IpcPipeline pipeline = new IpcPipeline(new AidlClientIpcPigeon.Factory(this));
        pipeline.addLast(new InboundHandlerAdapter() {
            @Override
            public void read(HandlerContext ctx, Object msg) throws Exception {
                if (msg instanceof IpcEvent) {
                    IpcEvent IpcEvent = (IpcEvent) msg;
                    if (IpcEvent.what == 0) {
                        Log.i("aaa", "Read 0: " + IpcEvent.text);
                        return;
                    }
                }
                ctx.fireRead(msg);
            }
        });
        pipeline.addLast(new InboundHandlerAdapter() {
            @Override
            public void read(HandlerContext ctx, Object msg) throws Exception {
                if (msg instanceof IpcEvent) {
                    IpcEvent IpcEvent = (IpcEvent) msg;
                    if (IpcEvent.what == 1) {
                        Log.i("aaa", "Read 1: " + IpcEvent.text);
                        IpcEvent.what = 2;
                        ctx.fireRead(IpcEvent);
                        return;
                    }
                }
                ctx.fireRead(msg);
            }
        });
        pipeline.addLast(new InboundHandlerAdapter() {
            @Override
            public void read(HandlerContext ctx, Object msg) throws Exception {
                if (msg instanceof IpcEvent) {
                    IpcEvent IpcEvent = (IpcEvent) msg;
                    if (IpcEvent.what == 2) {
                        Log.i("aaa", "Read 2: " + IpcEvent.text);
                        return;
                    }
                }
                ctx.fireRead(msg);
            }
        });
        pipeline.addLast(new InboundHandlerAdapter() {
            @Override
            public void read(HandlerContext ctx, Object msg) throws Exception {
                if (msg instanceof IpcEvent) {
                    IpcEvent IpcEvent = (IpcEvent) msg;
                    if (IpcEvent.what == 3) {
                        Log.i("aaa", "Read 3: " + IpcEvent.text);
                        IpcEvent.what = 0;
                        ctx.pipeline().fireRead(IpcEvent);
                        return;
                    }
                }
                ctx.fireRead(msg);
            }
        });

        mWhat = findViewById(R.id.view1);
        mText = findViewById(R.id.view2);
        findViewById(R.id.btn1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pipeline.connect();
            }
        });
        findViewById(R.id.btn2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pipeline.disconnect();
            }
        });
        findViewById(R.id.btn3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IpcEvent IpcEvent = new IpcEvent();
                IpcEvent.text = mText.getText().toString();
                try {
                    IpcEvent.what = Integer.parseInt(mWhat.getText().toString());
                } catch (NumberFormatException e) {
                }
                pipeline.write(IpcEvent);
            }
        });
    }
}

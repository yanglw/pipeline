package me.yanglw.android.ipc.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteCallbackList;
import android.os.RemoteException;

import me.yanglw.android.ipc.CallbackAidl;
import me.yanglw.android.ipc.IpcAidl;
import me.yanglw.android.ipc.IpcEvent;
import me.yanglw.java.pipeline.ipc.IpcPipeline;
import me.yanglw.java.pipeline.HandlerContext;
import me.yanglw.java.pipeline.InboundHandlerAdapter;

public class RemoteService extends Service {
    private RemoteCallbackList<CallbackAidl> mList = new RemoteCallbackList<CallbackAidl>() {
        @Override
        public void onCallbackDied(CallbackAidl callback) {
            mPipeline.fireInactive();
        }
    };

    private IBinder mIBinder = new IpcAidl.Stub() {
        @Override
        public void send(IpcEvent event) throws RemoteException {
            mPipeline.fireRead(event);
        }

        @Override
        public void register(CallbackAidl callback) throws RemoteException {
            mList.register(callback);
        }

        @Override
        public void unregister(CallbackAidl callback) throws RemoteException {
            mList.unregister(callback);
            checkClientCount();
        }
    };

    private final IpcPipeline mPipeline;

    public RemoteService() {
        mPipeline = new IpcPipeline(new AidlServiceIpcPigeon.Factory(this));
        mPipeline.addFirst(new InboundHandlerAdapter() {
            @Override
            public void read(HandlerContext ctx, Object msg) throws Exception {
                if (msg instanceof IpcEvent) {
                    if (((IpcEvent) msg).what == -1) {
                        return;
                    }
                }
                ctx.fireRead(msg);
            }
        });
        mPipeline.addLast(new InboundHandlerAdapter() {
            @Override
            public void read(HandlerContext ctx, Object msg) throws Exception {
                if (msg instanceof IpcEvent) {
                    ctx.write(msg);
                } else {
                    ctx.fireRead(msg);
                }
            }
        });
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mIBinder;
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
    }

    private void checkClientCount() {
        int count = mList.beginBroadcast();
        mList.finishBroadcast();
        if (count == 0) {
            mPipeline.fireInactive();
        }
    }

    void send2Client(IpcEvent event) {
        int count = mList.beginBroadcast();
        for (int i = 0; i < count; i++) {
            CallbackAidl item = mList.getBroadcastItem(i);
            try {
                item.send(event);
            } catch (RemoteException e) {
                mPipeline.fireExceptionCaught(e);
            }
        }
        mList.finishBroadcast();
    }
}

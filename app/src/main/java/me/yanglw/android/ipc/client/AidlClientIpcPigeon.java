package me.yanglw.android.ipc.client;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;

import java.util.LinkedList;
import java.util.List;

import me.yanglw.android.ipc.CallbackAidl;
import me.yanglw.android.ipc.IpcAidl;
import me.yanglw.android.ipc.IpcEvent;
import me.yanglw.java.pipeline.ipc.IpcPigeon;
import me.yanglw.java.pipeline.ipc.IpcPipeline;
import me.yanglw.android.ipc.service.RemoteService;

/**
 * Created by yanglw on 2018-10-25.
 */
public class AidlClientIpcPigeon implements IpcPigeon {
    private final Intent SERVICE_INTENT;

    private Context mContext;

    private List<IpcEvent> mList = new LinkedList<>();
    private IpcAidl mAidl;
    private ServiceConnection mServiceConnection;
    private CallbackAidl.Stub mCallback = new CallbackAidl.Stub() {
        @Override
        public void send(IpcEvent event) throws RemoteException {
            callIpcRead(event);
        }
    };
    private IpcPipeline mPipeline;

    private AidlClientIpcPigeon(Context context, IpcPipeline pipeline) {
        mContext = context.getApplicationContext();
        mPipeline = pipeline;
        SERVICE_INTENT = new Intent(mContext, RemoteService.class);
    }

    @Override
    public void bind() {}

    @Override
    public void close() {}

    @Override
    public void connect() {
        if (mServiceConnection != null) {
            return;
        }

        mServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, final IBinder service) {
                try {
                    service.linkToDeath(new IBinder.DeathRecipient() {
                        @Override
                        public void binderDied() {
                            service.unlinkToDeath(this, 0);
                            onServiceDied();
                        }
                    }, 0);
                } catch (RemoteException e) {
                    onServiceDied();
                    callExceptionCaught(e);
                    return;
                }

                mAidl = IpcAidl.Stub.asInterface(service);
                try {
                    mAidl.register(mCallback);
                    callIpcActive();
                    if (!mList.isEmpty()) {
                        for (IpcEvent event : mList) {
                            write(event);
                        }
                    }
                } catch (RemoteException e) {
                    onServiceDied();
                    callExceptionCaught(e);
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                onServiceDied();
            }
        };
        mContext.bindService(SERVICE_INTENT,
                             mServiceConnection,
                             Context.BIND_AUTO_CREATE);
        // 同时使用 bindService 和 startService 是因为：
        // 1. 使用 bindService 是为了实现进程间通讯。
        // 2. 使用 startService 是为了避免主进程关闭后 Service 因为没有连接而自动关闭的问题。
        mContext.startService(SERVICE_INTENT);
    }

    @Override
    public void disconnect() {
        if (mServiceConnection != null) {
            mContext.unbindService(mServiceConnection);
            mServiceConnection = null;
        }

        mContext.stopService(SERVICE_INTENT);
    }

    private void onServiceDied() {
        mAidl = null;
        disconnect();

        callIpcInactive();
    }

    @Override
    public void write(Object msg) {
        if (msg instanceof IpcEvent) {
            IpcEvent event = (IpcEvent) msg;
            if (mServiceConnection != null) {
                try {
                    mAidl.send(event);
                } catch (RemoteException e) {
                    mPipeline.fireExceptionCaught(e);
                }
            } else {
                mList.add(event);
            }
        }
    }

    @Override
    public void flush() {}

    private void callIpcActive() {
        mPipeline.fireActive();
    }

    private void callIpcInactive() {
        mPipeline.fireInactive();
    }

    private void callIpcRead(Object msg) {
        mPipeline.fireRead(msg);
    }

    private void callExceptionCaught(Throwable cause) {
        mPipeline.fireExceptionCaught(cause);
    }

    public static class Factory implements IpcPipeline.PigeonFactory {
        private Context mContext;

        public Factory(Context context) {
            mContext = context;
        }

        @Override
        public IpcPigeon create(IpcPipeline pipeline) {
            return new AidlClientIpcPigeon(mContext, pipeline);
        }
    }
}

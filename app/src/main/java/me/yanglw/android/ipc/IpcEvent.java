package me.yanglw.android.ipc;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by yanglw on 2018-10-25.
 */
public class IpcEvent implements Parcelable {
    public int what;
    public int arg1;
    public int arg2;
    public String text;

    public IpcEvent() {}

    protected IpcEvent(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.what);
        dest.writeInt(this.arg1);
        dest.writeInt(this.arg2);
        dest.writeString(this.text);
    }

    public void readFromParcel(Parcel in) {
        this.what = in.readInt();
        this.arg1 = in.readInt();
        this.arg2 = in.readInt();
        this.text = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<IpcEvent> CREATOR = new Creator<IpcEvent>() {
        @Override
        public IpcEvent createFromParcel(Parcel source) {
            return new IpcEvent(source);
        }

        @Override
        public IpcEvent[] newArray(int size) {
            return new IpcEvent[size];
        }
    };
}

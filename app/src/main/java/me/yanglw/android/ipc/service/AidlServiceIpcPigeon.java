package me.yanglw.android.ipc.service;

import me.yanglw.android.ipc.IpcEvent;
import me.yanglw.java.pipeline.ipc.IpcPigeon;
import me.yanglw.java.pipeline.ipc.IpcPipeline;

/**
 * Created by yanglw on 2018-10-25.
 */
public class AidlServiceIpcPigeon implements IpcPigeon {
    private RemoteService mContext;

    public AidlServiceIpcPigeon(RemoteService context) {
        mContext = context;
    }

    @Override
    public void bind() {}

    @Override
    public void close() {}

    @Override
    public void connect() {}

    @Override
    public void disconnect() {}

    @Override
    public void write(Object msg) {
        mContext.send2Client((IpcEvent) msg);
    }

    @Override
    public void flush() {}

    public static class Factory implements IpcPipeline.PigeonFactory {
        private RemoteService mContext;

        public Factory(RemoteService context) {
            mContext = context;
        }

        @Override
        public IpcPigeon create(IpcPipeline pipeline) {
            return new AidlServiceIpcPigeon(mContext);
        }
    }
}